# Generated by Django 4.0.3 on 2022-05-05 00:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200, unique=True)),
                ('closet_name', models.CharField(max_length=200)),
                ('section_number', models.CharField(max_length=200)),
                ('shelf_number', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Hats',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=100)),
                ('style_name', models.CharField(max_length=100)),
                ('color', models.CharField(max_length=100)),
                ('picture', models.URLField(blank=True, max_length=300, null=True)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo')),
            ],
        ),
    ]
