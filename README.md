# Wardrobify

Team:

* Brandon - Hats
* David - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

shoes/api: is a Django application with a Django project
shoes/poll: is a polling application that uses the Django resources in the RESTful API project.
Contains a script file shoes/poll/poller.py that you must implement to pull Bin data from the Wardrobe API.

Shoe resource should track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe
where it exists. 



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
