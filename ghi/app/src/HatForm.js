import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            picture: '',
            locations: []
        };
        this.handlefabricChange = this.handlefabricChange.bind(this);
        this.handlestyle_nameChange = this.handlestyle_nameChange.bind(this);
        this.handlecolorChange = this.handlecolorChange.bind(this);
        this.handlepictureChange = this.handlepictureChange.bind(this);
        this.handlelocationChange = this.handlelocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handlefabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handlestyle_nameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value })
    }
    handlecolorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handlepictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value })
    }
    handlelocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.locations;
        const locationId = data.location
        console.log(locationId)
        const HatUrl = `http://localhost:8090/api/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(data)
        const response = await fetch(HatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)
            const cleared = {
                fabric: '',
                style_name: '',
                color: '',
                picture: '',
                locations: []
            };
            this.setState(cleared);
        }
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ locations: data.locations });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handlefabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlestyle_nameChange} value={this.state.style} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlecolorChange} value={this.state.color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlepictureChange} value={this.state.picture} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handlelocationChange} value={this.state.location} name="location" required id="location" className="form-select">
                                    <option>Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HatForm;
